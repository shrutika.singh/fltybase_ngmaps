import { TestBed } from '@angular/core/testing';

import { RosAuthService } from './ros-auth.service';

describe('RosAuthService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RosAuthService = TestBed.get(RosAuthService);
    expect(service).toBeTruthy();
  });
});
