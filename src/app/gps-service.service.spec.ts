import { TestBed } from '@angular/core/testing';

import { GpsServiceService } from './gps-service.service';

describe('GpsServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GpsServiceService = TestBed.get(GpsServiceService);
    expect(service).toBeTruthy();
  });
});
