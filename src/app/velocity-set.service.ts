import { Injectable } from '@angular/core';
import ROSLIB from 'roslib';
import { RosAuthService } from './ros-auth.service';

@Injectable({
  providedIn: 'root'
})
export class VelocitySetService {
  constructor(
    private rosService: RosAuthService,
    ) { }
  ros = this.rosService.getros();

  velocitySet = new ROSLIB.Service({
    ros : this.ros,
    name : '/flytsim/navigation/velocity_set',
    serviceType : 'core_api/VelocitySet'
    });

    requestss = new ROSLIB.ServiceRequest({
      vx: -10.00,
      vy: 10.00,
      vz: 10.00,
      yaw_rate: 1.00,
      tolerance: 2.00,
      async: true,
      relative: false,
      yaw_rate_valid : true,
      body_frame : false
  });

  gpsData = new ROSLIB.Topic({
    ros : this.ros,
    name : '/flytsim/mavros/global_position/global',
    messageType : 'sensor_msgs/NavSatFix'
  });

   data = [];


   takeoff = new ROSLIB.Service({
    ros : this.ros,
    name : '/flytsim/navigation/take_off',
    serviceType : 'core_api/TakeOff'
    });

    requests = new ROSLIB.ServiceRequest({
      takeoff_alt: 3.00
  });

    velocityset_func() {
      this.velocitySet.callService(this.requestss, (result) => {
        if (result.success) {
          console.log('velocity set Success > ', result);
        }
    });
  }

  getdata() {
    return this.data;
  }

  takeOff() {
    this.takeoff.callService(this.requests,(result) => {
      if (result.success) {
      console.log('takeoff Success > ', result);
      }
    });
  }

}
