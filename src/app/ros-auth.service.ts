import { Injectable } from '@angular/core';
import ROSLIB from 'roslib';
@Injectable({
  providedIn: 'root'
})
export class RosAuthService {

  constructor() { }

  ros = new ROSLIB.Ros({
    url: 'wss://dev.flytbase.com/websocket',
  });

  getros() {
    return this.ros;
  }

}
