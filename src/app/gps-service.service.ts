import { Injectable } from '@angular/core';
import { RosAuthService } from './ros-auth.service';
import ROSLIB from 'roslib';

@Injectable({
  providedIn: 'root'
})
export class GpsServiceService {
  constructor(private rosService: RosAuthService) { }
  ros = this.rosService.getros();
  wav_lat_long: any;
  arr = [];
  waypointGet = new ROSLIB.Service({
        ros : this.ros,
        name : '/flytsim/navigation/waypoint_get',
        serviceType : 'core_api/WaypointGet'
    });
  waypoints: any;
  request = new ROSLIB.ServiceRequest({});
  waypointSet = new ROSLIB.Service({
    ros : this.ros,
    name : '/flytsim/navigation/waypoint_set',
    serviceType : 'core_api/WaypointSet'
  });
  waypointExecute = new ROSLIB.Service({
    ros : this.ros,
    name : '/flytsim/navigation/waypoint_execute',
    serviceType : 'core_api/WaypointExecute'
  });

  requestexecute = new ROSLIB.ServiceRequest({});

  requests = new ROSLIB.ServiceRequest({
  waypoints: [{
    frame :  2,
    command : 16,
    is_current : false,
    autocontinue : true,
    param1 : 0,
    param2 : 0,
    param3 : 0,
    param4 : 0,
    x_lat : 37.316391566474316,
    y_long : -122.21796480608215,
    z_alt : 5,
    }, {
      frame :  2,
      command : 16,
      is_current : false,
      autocontinue : true,
      param1 : 0,
      param2 : 0,
      param3 : 0,
      param4 : 0,
      x_lat : 37.316406498600195,
      y_long : -122.21795240086546,
      z_alt : 5,
      },
      {
        frame :  2,
        command : 16,
        is_current : false,
        autocontinue : true,
        param1 : 0,
        param2 : 0,
        param3 : 0,
        param4 : 0,
        x_lat : 37.3163979659572,
        y_long : -122.21793261957397,
        z_alt : 5,
        }, {
          frame :  2,
          command : 16,
          is_current : false,
          autocontinue : true,
          param1 : 0,
          param2 : 0,
          param3 : 0,
          param4 : 0,
          x_lat : 37.31638010073278,
          y_long : -122.21793060791721,
          z_alt : 5,
          }]
      });

    setwaypoint() {
      this.waypointSet.callService(this.requests, (result) => {
          this.requests.waypoints.map(elem =>{
            this.wav_lat_long = {
              lat: elem.x_lat,
              lng: elem.y_long,
            };
            this.arr.push(this.wav_lat_long);
          });
        });
      return this.arr;
    }
    getwaypoint() {
        this.waypointGet.callService(this.request, (result) => {
          result.waypoints.filter(elem=> elem.command === 16 && elem.frame === 3 ).map(elem =>{
                this.wav_lat_long = {
                  lat: elem.x_lat,
                  lng: elem.y_long,
                };
                this.arr.push(this.wav_lat_long);
            });
        });
      }

    executewaypoint() {
        this.waypointExecute.callService(this.requestexecute, (result) => {
          console.log('Result for service call on '
            + this.waypointExecute.name
            + ': '
            + result.success
            + result);
        });
    }
}


