import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import * as io from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class ClientSocketService {
  constructor() { }
  private socket = io('http://localhost:5000/'); // this is trying to make a connection therefore we need to listen to it in server

  // create observable to let client now that if the server is connected

    connectionComplete() {
          const observable = new Observable<String>((observer) =>
          this.socket.on('connect', (msg) => {
            //console.log(msg,'mssg');
            observer.next('connected to server');
            })
          );
          return observable;
    }

      rosconnention() {
        const observable = new Observable((observer) =>
        this.socket.on('ros_success', (msg) => {
          console.log(msg, 'mssg');
          observer.next(msg);
          })
        );
        return observable;
      }

      autheticateConnection(authdata) {
        console.log(authdata,'authdata');
        this.socket.emit('authenticate', authdata);
      }

}

