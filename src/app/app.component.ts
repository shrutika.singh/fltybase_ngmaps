import { Component } from '@angular/core';
import { ViewChild } from '@angular/core';
import { } from '@types/googlemaps';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor() {  }
  @ViewChild('gmap', { static: true }) gmapElement: any;
  map: google.maps.Map;
  latitude: any;
  longitude: any;
  poly: any;
  path: any;


  isHidden = false;

  ngOnInit() {

  }


}
