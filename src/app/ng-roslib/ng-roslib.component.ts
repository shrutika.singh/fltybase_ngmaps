import { Component, OnInit } from '@angular/core';
import ROSLIB from 'roslib';
import { RosAuthService } from '../ros-auth.service';
import { VelocitySetService } from '../velocity-set.service';
import { ClientSocketService } from '../client-socket.service';

@Component({
  selector: 'app-ng-roslib',
  templateUrl: './ng-roslib.component.html',
  styleUrls: ['./ng-roslib.component.css']
})
export class NgRoslibComponent implements OnInit {
  constructor(
        private rosService: RosAuthService,
        private velocitySetService: VelocitySetService,
        private  clientSocketService: ClientSocketService) {
            this.clientSocketService.connectionComplete()
            .subscribe((data) => {
                console.log('here at ng-ros', data);
            });
            this.clientSocketService.rosconnention()
            .subscribe((data) => {
                console.log('here at ros connection', data);
            });
      }

  ngOnInit() {
      const ros = this.rosService.getros();
      const authService = new ROSLIB.Service({
            ros: ros,
            name: '/websocket_auth',
        });
      const request = new ROSLIB.ServiceRequest({
          vehicleid: 'GnyqUBm6',
          authorization: 'Token ' + 'f31e3d0c768e475ea8e1de63e5fb5451f74749cf',
      });
      ros.on('connection', (id) => {
            console.log(id, 'Connected to web socket server from shrutika');
            authService.callService(request, (result) => {
            if (result.success) {
            console.log('Success > ', result);
            }
          });
        });
      }
      getdata3() {
        console.log('at getdata3', this.velocitySetService.getdata());
      }
      autheticate() {
        this.clientSocketService.autheticateConnection({vehicleid: 'GnyqUBm6' , token: 'f31e3d0c768e475ea8e1de63e5fb5451f74749cf'});
      }
  }



