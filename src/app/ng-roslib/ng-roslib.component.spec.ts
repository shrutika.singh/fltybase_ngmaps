import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgRoslibComponent } from './ng-roslib.component';

describe('NgRoslibComponent', () => {
  let component: NgRoslibComponent;
  let fixture: ComponentFixture<NgRoslibComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgRoslibComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgRoslibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
