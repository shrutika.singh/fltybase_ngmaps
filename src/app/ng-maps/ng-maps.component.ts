import { Input, Component } from '@angular/core';
import { ViewChild } from '@angular/core';
import { } from '@types/googlemaps';
import { VelocitySetService } from '../velocity-set.service';
import { GpsServiceService } from '../gps-service.service';
import { RosAuthService } from '../ros-auth.service';
import ROSLIB from 'roslib';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-ng-maps',
  templateUrl: './ng-maps.component.html',
  styleUrls: ['./ng-maps.component.css']
})
export class NgMapsComponent {
  @ViewChild('gmap', { static: true }) gmapElement: any;
  constructor(
    private velocitySetService: VelocitySetService,
    private gpsServiceService: GpsServiceService,
    private rosService: RosAuthService,
    ) {}
  // tslint:disable-next-line: no-input-rename
  @Input('parentfunc') public testfunc;
  map: google.maps.Map;
  latitude: any;
  longitude: any;
  location: any;
  poly: any;
  path: any;
  dronelongitude: any;
  dronelatitude: any;
  position0 = 2;
  position1 = 3;
  public dronepos = [];
  locations = [];
  isHidden = false;
  marker: any;
  message2: Subscription;
  Trail: any;
  image = {
    url: '../../assets/icons8-drone-96.png',
    size: new google.maps.Size(50, 50),
  };
  trail_lat_long: {};
  trail_arr: [];
  ros = this.rosService.getros();
  gpsData = new ROSLIB.Topic({
    ros : this.ros,
    name : '/flytsim/mavros/global_position/global',
    messageType : 'sensor_msgs/NavSatFix'
  });
  ngOnInit() {
  }

  ngAfterContentInit() {
    const mapProp = {
      center: new google.maps.LatLng( 37.0855598449707,  -122.47698211669922),
      zoom: 8, //14
    };
    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
    this.message2 = this.gpsData.subscribe((dronepos) => {
      this.dronelatitude = dronepos.latitude;
      this.dronelongitude = dronepos.longitude;
      this.location = new google.maps.LatLng(this.dronelatitude, this.dronelongitude);
      this.marker.setPosition(this.location );
    }, err => {
      console.log('err at gps sucbsribe', err);
    });
    this.marker = new google.maps.Marker({
      position: this.location,
      map: this.map,
      icon: {
        path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
        strokeColor: 'red',
        fillColor: 'blue',
        scale: 5,
        size: new google.maps.Size(500, 50),
      },
      title: 'drone',
      draggable: true,
    });
    this.map.addListener('click', (mapsMouseEvent) => {
     console.log(mapsMouseEvent.latLng.toString());
    });
  }

  takeoff() {
    this.velocitySetService.takeOff();
  }
  
  getdata2() {
    this.dronepos = (this.velocitySetService.getdata());
    this.dronelatitude = this.dronepos.latitude;
    this.dronelongitude = this.dronepos.longitude;
  }
  moveMarker() {
    this.marker.addListener('click', this.moveMarker22());
  }
  moveMarker22() {
  this.dronepos = (this.velocitySetService.getdata());
    this.dronelatitude = this.dronepos.latitude + 2;
    this.dronelongitude = this.dronepos.longitude + 0.4;
    const latlng = new google.maps.LatLng(this.dronelatitude, this.dronelongitude);
    this.marker.setPosition(latlng);
  }

  setCenter() {
    this.map.setCenter(this.location);
    this.map.setZoom(14);
  }

  showLine() {
    this.map.setCenter(new google.maps.LatLng(this.latitude, this.longitude));
    const flightPlanCoordinates = this.gpsServiceService.setwaypoint();
    const flightPath = new google.maps.Polyline({
      path: flightPlanCoordinates,
      geodesic: true,
      strokeColor: '#FF0000',
      strokeOpacity: 1.0,
      strokeWeight: 2,
    });
    flightPath.setMap(this.map);
  }

 toggleMap() {
    this.isHidden = !this.isHidden;
    this.gmapElement.nativeElement.hidden = this.isHidden;
  }

  getWayPoint() {
    const test = this.gpsServiceService.getwaypoint();
    this.locations = test;
    this.showWayPointMarkers();
  }
  setWayPoint() {
    const newwaypoints = this.gpsServiceService.setwaypoint();
    this.locations = newwaypoints;
    this.showWayPointMarkers();
  }

  executeWayPoint() {
    this.gpsServiceService.executewaypoint();
  }

  pauseMission() {
    this.message2 = this.gpsData.unsubscribe();
  }
  showWayPointMarkers() {
      const markers = this.locations.map((location ) => {
        return new google.maps.Marker({
          position: location,
          map: this.map,
          icon: {
            path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
            strokeColor: 'blue',
            fillColor: 'blue',
            scale: 5,
            size: new google.maps.Size(500, 50),
          },
        });
      });
    }
}
