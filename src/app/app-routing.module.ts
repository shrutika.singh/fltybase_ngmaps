import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { NgMapsComponent } from './ng-maps/ng-maps.component';
import { NgRoslibComponent } from './ng-roslib/ng-roslib.component';
import { ChatPageComponent } from './chat-page/chat-page.component';
const routes: Routes = [
//  { path: 'map', component: NgMapsComponent},
  { path: 'ros', component: NgRoslibComponent},
  { path: 'chat', component: ChatPageComponent}
];


@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
