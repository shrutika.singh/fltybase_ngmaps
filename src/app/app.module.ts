import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { NgMapsComponent } from './ng-maps/ng-maps.component';
import { AppRoutingModule } from './app-routing.module';
import { NgRoslibComponent } from './ng-roslib/ng-roslib.component';
import { RosAuthService } from './ros-auth.service';
import { VelocitySetService } from './velocity-set.service';
import { GpsServiceService } from './gps-service.service';
import {  ChatServiceService } from './chat-service.service';
import { ChatPageComponent } from './chat-page/chat-page.component';

@NgModule({
  declarations: [
    AppComponent,
    NgMapsComponent,
    NgRoslibComponent,
    ChatPageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [RosAuthService, VelocitySetService , GpsServiceService, ChatServiceService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
