import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import * as io from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class ChatServiceService {
  constructor() { }
  private socket = io('http://localhost:3000/'); // this is trying to make a connection therefore we need to listen to it in server

  // create observable to let client now that if the server is connected
  connectionComplete() {
    const observable = new Observable<String>((observer) =>
    this.socket.on('connection', (msg) => {
      console.log(msg);
       observer.next(msg,'msggggg');
      })
    );
    return observable;
  }

  // emit the data of user on join
  joinRoom(data) {
    console.log( 'here', data);
    this.socket.emit('join', data);
  }

  // create an observable to let users in the room to know if any new users joins
  newUserJoined() {
    return Observable.create((observer) => {
    this.socket.on('newuserjoined', (datas) => {
       observer.next(datas);
    });
   });
  }

  leaveRoom(data) {
    console.log('here leave')
    this.socket.emit('leave', data);
  }

  userleftRoom() {
    console.log("at left")
    const observable = new Observable <{user: String , message: String}> ((observer) => {
      this.socket.on('userleftroom', datas => {
        observer.next(datas);
      });
    });
    return observable;
  }

}

