import { Component, OnInit } from '@angular/core';
import {  ChatServiceService } from '../chat-service.service';

@Component({
  selector: 'app-chat-page',
  templateUrl: './chat-page.component.html',
  styleUrls: ['./chat-page.component.css']
})
export class ChatPageComponent implements OnInit {
  user: String;
  room: String;
  messageArray: Array<{user: String, message: String}> = [];
  constructor(private chatservice: ChatServiceService) {
    this.chatservice.connectionComplete()
    .subscribe(data => {
      console.log(data);
    });

    this.chatservice.newUserJoined()
    .subscribe(datas => {
      console.log('msg', this.messageArray);
      this.messageArray.push(datas);
    });

    this.chatservice.userleftRoom()
    .subscribe(data => {
      console.log(data.user + 'left the room');
      console.log('msg', this.messageArray);
      this.messageArray.push(data);
    });
  }

  ngOnInit() {
    this.chatservice.newUserJoined();
  }

  join() {
    this.chatservice.joinRoom({user: this.user, room: this.room});
  }

  leave() {
    this.chatservice.leaveRoom({user: this.user, room: this.room});
  }
}
