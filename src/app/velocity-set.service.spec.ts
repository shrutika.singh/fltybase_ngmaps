import { TestBed } from '@angular/core/testing';

import { VelocitySetService } from './velocity-set.service';

describe('VelocitySetService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VelocitySetService = TestBed.get(VelocitySetService);
    expect(service).toBeTruthy();
  });
});
